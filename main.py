import json, fileinput

INPUTFILE_LOC = "./scan_result.json"
LOW_SEV = 0
MED_SEV = 0
HIGH_SEV = 0
CRIT_SEV = 0
UNK_SEV = 0
SEV_DETAILS = ""
SEV_RANK_LIST = []
SEV_TABLE_TEMPLATE = """
                <tr>
                <td>{0}</td>
                <td>{1}</td>
                <td><a href="{2}">Link</a></td>
                <td><b>{3}</b></td>
            </tr>
"""
SEV_MAP = {
    "Low": 4,
    "Medium": 3,
    "High": 2,
    "Critical": 1,
    "Unknown": 5
}


with open(INPUTFILE_LOC, 'r') as file:
    data = file.read().replace('\n', '')

json_file = json.loads(data)
SEV_RANK = 0
for match in json_file['matches']:
    if match['vulnerability']['severity'] == "Low":
        LOW_SEV += 1
    if match['vulnerability']['severity'] == "Medium":
        MED_SEV += 1
    if match['vulnerability']['severity'] == "High":
        HIGH_SEV += 1
    if match['vulnerability']['severity'] == "Critical":
        CRIT_SEV += 1
    if match['vulnerability']['severity'] == "Unknown":
        UNK_SEV += 1
    #print(match['vulnerability']['id'], match['vulnerability']['severity'], match['vulnerability']['description'], match['vulnerability']['dataSource'])
    SEV_RANK_LIST.append([SEV_MAP[match['vulnerability']['severity']], match['vulnerability']['id'], match['vulnerability']['description'], match['vulnerability']['dataSource'], match['vulnerability']['severity']])


SORTED_SEV_RANK_LIST = sorted(SEV_RANK_LIST, key=lambda x: x[0])
for row in SORTED_SEV_RANK_LIST:
    mod_tbl_temp =SEV_TABLE_TEMPLATE.format(row[1], row[2], row[3], row[4])
    SEV_DETAILS = SEV_DETAILS + mod_tbl_temp


with open('./template.html', 'r') as html_template:
    template_data = html_template.read()

with open('./scan_result.html', 'w') as outfile:
    outfile.write(template_data.format(str(LOW_SEV), str(MED_SEV), str(HIGH_SEV), str(CRIT_SEV), str(UNK_SEV), SEV_DETAILS))

